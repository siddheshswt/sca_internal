﻿using NuGet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace pckg_version
{
    public partial class Form1 : Form
    {
        string sSelectedfolder;
        string[] fileArray;
        DialogResult result;
        string logPath = @"E:\NugetUtilityLog.txt";


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            outputmsg.Text = "Please select root folder of solution";
            ExportNuget.Enabled = false;

            if (!File.Exists(logPath))
            {
                using (FileStream fs = File.Create(logPath))
                {
                    // Add some text to file
                    Byte[] title = new UTF8Encoding(true).GetBytes("Log File is get created..");
                    fs.Write(title, 0, title.Length);
                    byte[] author = new UTF8Encoding(true).GetBytes("Ganesh More");
                    fs.Write(author, 0, author.Length);
                }

            }
        }
        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Do something proper to CloseButton.
            if (MessageBox.Show("Do you want to closed NuGet utility?", "Nuget Package List",
   MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // Cancel the Closing event from closing the form.
                e.Cancel = true;

                // Call method to save file...
            }
            else
            {
                System.Windows.Forms.Application.Exit();
                Environment.Exit(0);
            }

        }


        public static class Log
        {
            public static void write(string message, Exception ex, string logPath)
            {
                string text = ex == null ? message : ex.ToString();
                using (StreamWriter sw = new StreamWriter(logPath, true))
                {
                    sw.WriteLine(text);
                }
            }
        }
        private void ExportNuget_Click(object sender, EventArgs e)
        {
            ExportNuget.Enabled = false;
            PackageVersions.Rows.Clear();
            PackageVersions.Refresh();
            PackageVersions.Visible = true;
            outputmsg.Text = "Loading....Exporting Nuget package for given solution.";
            Log.write("/*****************NuGet Package List extraction started  " + DateTime.Now.ToString() + "*****************/", null, logPath);
            IPackageRepository repo = PackageRepositoryFactory.Default.CreateRepository("https://packages.nuget.org/api/v2");

            foreach (var file in fileArray)
            {
                var pkgRefpath = file;

                PackageReferenceFile nugetPkgConfig = new PackageReferenceFile(pkgRefpath);
                IEnumerable<PackageReference> allPackages = nugetPkgConfig.GetPackageReferences();
                //List<IPackage> Packages = new List<IPackage>();
                Log.write(allPackages.Count() + " NuGet Packages found in the given solution", null, logPath);
                try
                {
                    foreach (var mypakage in allPackages)
                    {

                        //Connect to the official package repository
                        Log.write("/**" + mypakage.Id + " found in the solution" + "**/", null, logPath);
                        //Get the list of all NuGet packages with ID 'EntityFramework'       
                        var latestPackages = repo.FindPackagesById(mypakage.Id).Where(item => item.IsReleaseVersion()== true).Max(p => p.Version);

                        PackageVersions.Rows.Add(mypakage.Id, mypakage.Version.ToString(), latestPackages);
                        Log.write("/**" + mypakage.Id + " copy to excel sheet." + "**/", null, logPath);
                    }

                }
                catch (Exception ex)
                {
                    Log.write("NuGetAPI ConnectivityError Exception get in importing packages...", ex, logPath);
                }
            }
            for (int i = 0; i < PackageVersions.Rows.Count - 1; i++)
            {
                int k = 0;
                for (int j = 0; j < PackageVersions.Rows.Count - 1; j++)
                {
                    if (PackageVersions.Rows[i].Cells[0].Value.ToString() == PackageVersions.Rows[j].Cells[0].Value.ToString())
                    {
                        if (k != 0)
                        {
                            PackageVersions.Rows.RemoveAt(j);
                        }
                        k++;

                    }
                }
            }
            PackageVersions.Sort(PackageVersions.Columns[0], ListSortDirection.Ascending);
            try
            {
                if (PackageVersions.Rows.Count > 1)
                {
                    Microsoft.Office.Interop.Excel.Application xlApp;
                    Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                    Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                    object misValue = System.Reflection.Missing.Value;

                    xlApp = new Microsoft.Office.Interop.Excel.Application();
                    xlWorkBook = xlApp.Workbooks.Add(misValue);
                    xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    for (j = 0; j <= 2; j++)
                    {
                        Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                        rng.Font.Bold = true;
                        rng.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Aqua);
                        rng.BorderAround();
                    }
                    i = 0;
                    j = 0;
                    xlWorkSheet.Cells[i + 1, j + 1] = "Package Name";
                    xlWorkSheet.Cells[i + 1, j + 2] = "Installed Version";
                    xlWorkSheet.Cells[i + 1, j + 3] = "Latest Version";
                    //xlWorkSheet.Cells[i + 1, j + 4] = "Description";
                    //xlWorkSheet.Cells[i + 1, j + 5] = "Dependent Nuget Packages";

                    for (i = 1; i <= PackageVersions.RowCount; i++)
                    {
                        for (j = 0; j <= PackageVersions.ColumnCount - 1; j++)
                        {
                            Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i, j + 1];
                            rng.BorderAround();
                            xlWorkSheet.Columns.AutoFit();
                            xlWorkSheet.Rows.AutoFit();
                            DataGridViewCell cell = PackageVersions[j, i - 1];
                            if (j > 1)
                            {
                                if (PackageVersions[j - 1, i - 1].Value != null && PackageVersions[j, i - 1].Value != null)
                                {
                                    if (PackageVersions[j - 1, i - 1].Value.ToString() == PackageVersions[j, i - 1].Value.ToString())
                                    {
                                        Microsoft.Office.Interop.Excel.Range rng1 = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                                        Microsoft.Office.Interop.Excel.Range rng2 = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j];
                                        rng2.Font.Bold = rng1.Font.Bold = true;
                                        rng2.Interior.Color = rng1.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                                        rng1.BorderAround();
                                    }
                                }
                            }
                            //if (cell.Value != null)
                            //{
                            //    xlWorkSheet.Cells[i + 1, j + 1] = cell.Value.ToString();
                            //}
                            //else
                            //{
                            //    xlWorkSheet.Cells[i + 1, j + 1] = "";
                            //}
                            xlWorkSheet.Cells[i + 1, j + 1] = cell.Value != null ? cell.Value.ToString() : "";
                        }
                    }
                    string lastFolderName = Path.GetFileName(Path.GetDirectoryName(sSelectedfolder));
                    string filename = "E:\\Nuget_List_" + lastFolderName + "_" + System.DateTime.Now.ToString("dd-MMM-yyyy_hh-mm");

                    xlWorkBook.SaveAs(filename, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();

                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);

                    outputmsg.Text = "From Given Solution " + PackageVersions.RowCount + " Nuget packages exported in Excel file, you can find the file in D drive as filename as" + filename;
                    result = MessageBox.Show("From Given Solution " + PackageVersions.RowCount + " NuGet packages exported in Excel file, you can find the file in D drive as filename as" + filename);
                    Log.write("From Given Solution " + PackageVersions.RowCount + " NuGet packages exported in Excel file, you can find the file in D drive as filename as" + filename, null, logPath);
                }

                else
                {
                    //outputmsg.Text = "No Nuget to export";
                    result = MessageBox.Show("No NuGet Package used in given solution!");
                    Log.write("No NuGet Package used in given solution!", null, logPath);
                }

                if (result == DialogResult.OK)
                {
                    Reset();
                    //System.Windows.Forms.Application.Exit();
                }
            }
            catch (Exception ex)
            {
                Log.write("Excception occured in exporting excel sheet", ex, logPath);
            }
        }

        private void SelectFolder_Click(object sender, EventArgs e)
        {

            try
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    sSelectedfolder = folderBrowserDialog1.SelectedPath.ToString();
                    fileArray = Directory.GetFiles(sSelectedfolder, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith("packages.config")).ToArray(); ;
                    ExportNuget.Enabled = true;
                    ExportNuget.Visible = true;
                    PackageVersions.DataSource = null;
                    PackageVersions.Rows.Clear();
                    PackageVersions.Refresh();
                    PackageVersions.Visible = true;
                }
            }
            catch (FileNotFoundException ex)
            {
                Log.write("FileNotFoundException exception in the getting file path method, thus no extract list would be extract", ex, logPath);
            }
            catch (PathTooLongException ex)
            {
                Log.write("PathTooLongException exception in the SendPatchingEmail method, thus no notification email would be sent", ex, logPath);
            }
            catch (Exception ex)
            {
                Log.write("Excception occured in exporting excel sheet", ex, logPath);
            }
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {


        }

        public void Reset()
        {
            PackageVersions.DataSource = null;
            PackageVersions.Rows.Clear();
            PackageVersions.Refresh();
            PackageVersions.Visible = false;
            ExportNuget.Visible = false;
            outputmsg.Text = "Please select root folder of solution.";
        }
        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
