﻿///////////////////////////////////////////////////////////////////////////////
Developer: Siddhesh Sawant
Created Date: 05-Feb-2018
Purpose: Send the count details of Incident, SR and Problem tickets.
//////////////////////////////////////////////////////////////////////////////

*STEPS TO EXECUTE*

1.  Deployed location should contain 4 folders (Input, Archive, Log and MasterFile) along with project exe, library dll and config file.
		a. Library.dll
		b. TicketDetails.exe
		c. TicketDetails.exe.config
		d. Input
		e. Archive
		f. MasterFile
		g. Log

2.  Specify the folders/deploy path (Input, Archive, Log and MasterFile) in the config file. 
	for e.g. 
	<add key="FilePath" value="d:\Scheduler\SCAInternal\TicketStausDetails\Input"/>
    <add key="ArchivePath" value="d:\Scheduler\SCAInternal\TicketStausDetails\Archive"/>
    <add key="MasterExelPath" value ="d:\Scheduler\SCAInternal\TicketStausDetails\MasterFile"/>
    <add key="Log" value="d:\Scheduler\SCAInternal\TicketStausDetails\Log\Log.txt" />

3. Input folder contains the input excel file that is to be processed. There should be one file at a time.

4. Archive folder contains processed file.

5. MasterFile contains List.xlsx, having all the master data requires to process input file. This contains 6 sheet and should not be rename or change the order. 
   The master data can be added/modify here.

6. Log folder conatins Log.txt for maintaing the log of each run.

7. Mail To and Cc and the subject for each mail can be set in the config file.

8. Mail Formats can be found in SampleMailFormats folder in the solution.