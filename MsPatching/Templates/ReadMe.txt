/##############################################################################################################/
					README - MS Patching Scheduler

/##############################################################################################################/

###############################################################################################################
	* Project - Essity - Sogeti Sweden
	* Purpose - To send the notification emails for the MS Patching activity based on pacthing calendar.
	* Environments covered - Dev3, Test3, UAT, Dev2, Test2, Prod
	* Developed By: Snehal Dhode
	* Execution Frequency: Approx. 3 times/month
	* Input for data: As per SDL TRIDION patching Schedule
	
###############################################################################################################
----------------------------------------------------------------------------------------------------------------
Process decription:
----------------------------------------------------------------------------------------------------------------
•	MS-patch activity schedule Monthly for All Environment (Dev, Test ,UAT ,Prod).
•	Schedule for MS patch activity is as per given in SDL Tridion Patching schedule.
•	As per schedule for MS Patch, Mail alert need to be send for list of recipients and required authorities
    before one working day prior to MS Patch activity
•	Mail alert are based on below Email-template which include Date and timeline for patch activity as given in schedule. 

----------------------------------------------------------------------------------------------------------------
SDL TRIDION patching Schedule::
----------------------------------------------------------------------------------------------------------------
•	DEV3/ TEST3		 -- Activity on 2nd Thursday of every month -- Reminder a day prior i.e on Wednesday 
•	UAT 			 -- Activity on 2nd Sunday of every month	-- Reminder 2 days prior i.e. on Friday
•	Dev2/Test2/PROD	 -- Activity on 4th Monday of every month	-- Reminder 3 days prior i.e. on Friday

----------------------------------------------------------------------------------------------------------------
Mandatory Folder structure:
----------------------------------------------------------------------------------------------------------------
	1.Templates:
		a.	Should be having HTML email templates 
		b.	Should be having Patching calendar (in excelsheet format) named in below format 
			For example, Patching calender-2018.xlsx
		c.	Should be having To and CC distribution files
		
	2.Logs:
		Should be present, wihin it logs would be generated.

----------------------------------------------------------------------------------------------------------------
Templates used for sending emails:
----------------------------------------------------------------------------------------------------------------
1.	Dev3Test3EmailTemplate.html
2.	UATEmailTemplate.html
3. 	ProdDev3Test3EmailTemplate.html

** The above 3 templates are used for sending the emails fr those respective environments.
** If any chnages need to be performed in email content, then the respective HTML template should be updated.
** These HTML files have the placedholders in curly braces (for example, {0}, {1}, {2}) do not replace those
   placeholders while updating any of the HTML template. These placeholders are used for patching dates.
  
----------------------------------------------------------------------------------------------------------------
Subject Lines for Patching emails:
----------------------------------------------------------------------------------------------------------------
** Email ubject lines for patching emails to be sent for each environment are specified in the config file.
** Any changes in email subject lines will need config update.
** These email subject lines have the placedholders in curly braces(for example, {0}, {1}) do not replace those
   placeholders while updating any of subject lines in config files. These placeholders are used for patching
   dates.

----------------------------------------------------------------------------------------------------------------
Distribution List for emails:
----------------------------------------------------------------------------------------------------------------
** Currently 2 distribution lists are used for patching namely TO and CC lists.
** ToDistributionList.txt files holds emails ids of TO recipients
** CCDistributionList.txt files holds emails ids of CC recipients

In case of any new addition of email id, it should be done on new line of respective distribution list file.
----------------------------------------------------------------------------------------------------------------
Patching calendar:
----------------------------------------------------------------------------------------------------------------

** Patching calendar should be in the format as --> Patching calender-{current-year}.xlsx
   For example, Patching calender-2018.xlsx
** It should be placed in the templates folder
** It should hold date entries for all 12 months o fthat year.
** It should hold date entries for DEV3/Test3, UAT and Prod/Dev2/Test2 environments.

----------------------------------------------------------------------------------------------------------------
Logs:
----------------------------------------------------------------------------------------------------------------

Logs.txt file generated in /Logs folder could be used to monitor every execution of this schedular. this file
should be monitored in case of any issue,it shall be holding every run logging entry as well as errors, if any.
